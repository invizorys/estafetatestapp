package com.invizorys.estafetatestapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.invizorys.estafetatestapp.R;
import com.invizorys.estafetatestapp.activity.TaskActivity;
import com.invizorys.estafetatestapp.entity.SurveyTask;

import java.util.List;

/**
 * Created by Paryshkura Roman on 21.05.2015.
 */
public class GridViewAdapter extends BaseAdapter {

    private List<SurveyTask> surveyTasks;
    private Context context;

    public GridViewAdapter(Context context, List<SurveyTask> surveyTasks) {
        this.context = context;
        this.surveyTasks = surveyTasks;
    }

    @Override
    public int getCount() {
        return surveyTasks.size();
    }

    @Override
    public SurveyTask getItem(int position) {
        return surveyTasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.grid_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.textviewId = (TextView) convertView.findViewById(R.id.textview_id);
            viewHolder.textviewNumber = (TextView) convertView.findViewById(R.id.textview_number);
            viewHolder.textviewCreationDate = (TextView) convertView.findViewById(R.id.textview_creation_date);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textviewId.setText("id: " + surveyTasks.get(position).getId());
        viewHolder.textviewNumber.setText("number: " + surveyTasks.get(position).getNumber());
        viewHolder.textviewCreationDate.setText("creationDate: " + surveyTasks.get(position).getCreationDate());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SurveyTask surveyTask = getItem(position);
                Intent intent = new Intent(context, TaskActivity.class);
                intent.putExtra("surveyTask", surveyTask);
                context.startActivity(intent);
            }
        });
        return convertView;
    }

    static class ViewHolder {
        TextView textviewId;
        TextView textviewNumber;
        TextView textviewCreationDate;
    }
}
