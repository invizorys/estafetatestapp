package com.invizorys.estafetatestapp.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.invizorys.estafetatestapp.R;
import com.invizorys.estafetatestapp.entity.SurveyTask;

public class TaskActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        SurveyTask surveyTask = (SurveyTask) getIntent().getSerializableExtra("surveyTask");
        ((TextView) findViewById(R.id.textview_id)).setText("id: " + surveyTask.getId());
        ((TextView) findViewById(R.id.textview_number)).setText("number: " + surveyTask.getNumber());
        ((TextView) findViewById(R.id.textview_actual_start_date)).setText("actual start date: " + surveyTask.getActualStartDate());
        ((TextView) findViewById(R.id.textview_actual_end_date)).setText("actual end date: " + surveyTask.getActualEndDate());
        ((TextView) findViewById(R.id.textview_creation_date)).setText("creation date: " + surveyTask.getCreationDate());
        ((TextView) findViewById(R.id.textview_executor_id)).setText("executor id: " + surveyTask.getExecutorId());
        ((TextView) findViewById(R.id.textview_kind_id)).setText("kind id: " + surveyTask.getKindId());
        ((TextView) findViewById(R.id.textview_brand_id)).setText("brand id: " + surveyTask.getBrandId());
    }
}
