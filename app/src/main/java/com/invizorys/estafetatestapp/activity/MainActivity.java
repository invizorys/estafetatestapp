package com.invizorys.estafetatestapp.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SlidingPaneLayout;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.invizorys.estafetatestapp.R;
import com.invizorys.estafetatestapp.adapter.GridViewAdapter;
import com.invizorys.estafetatestapp.api.EstafetaApi;
import com.invizorys.estafetatestapp.api.RetrofitCallback;
import com.invizorys.estafetatestapp.api.ServiceGenerator;
import com.invizorys.estafetatestapp.entity.SurveyTask;
import com.invizorys.estafetatestapp.util.Network;

import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends Activity implements View.OnClickListener {

    private ProgressDialog progressDialog;
    private ImageButton buttonMenu;
    private SlidingPaneLayout slidingPaneLayout;
    private TextView textviewMenuInfo;
    private LinearLayout containerSync;
    private TableLayout containerTask;

    public static final String LOGIN = "admin";
    public static final String COMPANY_ID = "9F346DDB-8FF8-4F42-8221-6E03D6491756";
    public static final String PASSWORD = "1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        containerTask = (TableLayout) findViewById(R.id.container_task);
        slidingPaneLayout = (SlidingPaneLayout) findViewById(R.id.sliding_pane_layout);
        textviewMenuInfo = (TextView) findViewById(R.id.textview_menu_info);
        containerSync = (LinearLayout) findViewById(R.id.container_sync);
        containerSync.setOnClickListener(this);

        buttonMenu = (ImageButton) findViewById(R.id.button_menu);
        buttonMenu.setOnClickListener(this);

        slidingPaneLayout.setPanelSlideListener(new SlidingPaneLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelOpened(View panel) {
                buttonMenu.setImageResource(R.drawable.arrow_left);
                textviewMenuInfo.setText(getString(R.string.hide_menu));
            }

            @Override
            public void onPanelClosed(View panel) {
                buttonMenu.setImageResource(R.drawable.arrow_right);
                textviewMenuInfo.setText("");
            }
        });
//        getTasks();
        addTasks();
    }

    private void getTasks() {
        if (Network.isOnline(this)) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading");
            progressDialog.setCancelable(false);
            progressDialog.show();

            EstafetaApi estafetaApi = ServiceGenerator.createService(EstafetaApi.class, EstafetaApi.BASE_URL,
                    LOGIN, COMPANY_ID, PASSWORD);

            String userId = "";
            String effectivePlannedStartDate = "";
            String expirationPlannedStartDate = "";
            int startRowVersion = 0;
            int stateId = 4;
            int count = 100;
            estafetaApi.getTableSurveyTasks(userId, effectivePlannedStartDate, expirationPlannedStartDate,
                    startRowVersion, stateId, count, new RetrofitCallback<List<SurveyTask>>(MainActivity.this) {
                        @Override
                        public void success(List<SurveyTask> surveyTasks, Response response) {
                            ((GridView) findViewById(R.id.container)).setAdapter(
                                    new GridViewAdapter(MainActivity.this, surveyTasks));
                            progressDialog.cancel();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            super.failure(error);
                            progressDialog.cancel();
                        }
                    });
        } else {
            Toast.makeText(this, getString(R.string.check_your_internet_connection), Toast.LENGTH_SHORT).show();
        }
    }

    private void addTasks() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (int i = 0; i < 5; i++) {
            TableRow tableRow = (TableRow) inflater.inflate(R.layout.table_row, null);

            TextView textViewVin = (TextView) tableRow.findViewById(R.id.textview_vin);
            textViewVin.setText("VF1lSRAER");

            TextView textViewBrand = (TextView) tableRow.findViewById(R.id.textview_brand);
            textViewBrand.setText("Renault");

            TextView textViewModel = (TextView) tableRow.findViewById(R.id.textview_model);
            textViewModel.setText("Logan");

            TextView textViewTransport = (TextView) tableRow.findViewById(R.id.textview_transport);
            textViewTransport.setText("AA5867EP");

            TextView textViewDriver = (TextView) tableRow.findViewById(R.id.textview_driver);
            textViewDriver.setText("Лисий В.Д.");

            containerTask.addView(tableRow);

            TableRow tableRowSeparator = (TableRow) inflater.inflate(R.layout.table_row_separator, null);
            containerTask.addView(tableRowSeparator);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_menu:
                if (slidingPaneLayout.isOpen()) {
                    slidingPaneLayout.closePane();
                } else {
                    slidingPaneLayout.openPane();
                }
                break;
            case R.id.container_sync:
                getTasks();
                break;
        }
    }
}
