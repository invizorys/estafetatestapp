package com.invizorys.estafetatestapp.entity;

import java.io.Serializable;

/**
 * Created by Paryshkura Roman on 21.05.2015.
 */
public class SurveyTask implements Serializable{

    private Integer Id;
    private String Number;
    private Object PlannedStartDate;
    private Object PlannedEndDate;
    private String ActualStartDate;
    private String ActualEndDate;
    private String CreationDate;
    private String ExecutorId;
    private Integer KindId;
    private Integer IntRowVer;
    private Integer StateId;
    private String Vin;
    private String BrandId;
    private Object ExecutorUnitId;
    private String AssigneeId;
    private Object GroupTaskId;
    private Boolean IsGroup;
    private Integer TypeId;
    private Integer ImplementationTypeId;
    private String ModelCodeId;
    private Object AssigneeUnitId;
    private String SurveyPointId;
    private Integer VehiclePosition;
    private Integer GasAmount;
    private Object IsInTraffic;
    private Integer Speedometer;
    private Object VehiclePositionDescription;
    private String SignUserId;
    private Object StateNumber;
    private String CarrierId;
    private String ShippingAgentId;
    private String FirstDriverId;
    private Object SecondDriverId;
    private Integer TransportationTypeId;
    private Integer TransportId;
    private String FromUnitId;
    private String ToUnitId;
    private Object FromUserId;
    private Object ToUserId;
    private Object SignUnitId;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public Object getPlannedStartDate() {
        return PlannedStartDate;
    }

    public void setPlannedStartDate(Object plannedStartDate) {
        PlannedStartDate = plannedStartDate;
    }

    public Object getPlannedEndDate() {
        return PlannedEndDate;
    }

    public void setPlannedEndDate(Object plannedEndDate) {
        PlannedEndDate = plannedEndDate;
    }

    public String getActualStartDate() {
        return ActualStartDate;
    }

    public void setActualStartDate(String actualStartDate) {
        ActualStartDate = actualStartDate;
    }

    public String getActualEndDate() {
        return ActualEndDate;
    }

    public void setActualEndDate(String actualEndDate) {
        ActualEndDate = actualEndDate;
    }

    public String getCreationDate() {
        return CreationDate;
    }

    public void setCreationDate(String creationDate) {
        CreationDate = creationDate;
    }

    public String getExecutorId() {
        return ExecutorId;
    }

    public void setExecutorId(String executorId) {
        ExecutorId = executorId;
    }

    public Integer getKindId() {
        return KindId;
    }

    public void setKindId(Integer kindId) {
        KindId = kindId;
    }

    public Integer getIntRowVer() {
        return IntRowVer;
    }

    public void setIntRowVer(Integer intRowVer) {
        IntRowVer = intRowVer;
    }

    public Integer getStateId() {
        return StateId;
    }

    public void setStateId(Integer stateId) {
        StateId = stateId;
    }

    public String getVin() {
        return Vin;
    }

    public void setVin(String vin) {
        Vin = vin;
    }

    public String getBrandId() {
        return BrandId;
    }

    public void setBrandId(String brandId) {
        BrandId = brandId;
    }

    public Object getExecutorUnitId() {
        return ExecutorUnitId;
    }

    public void setExecutorUnitId(Object executorUnitId) {
        ExecutorUnitId = executorUnitId;
    }

    public String getAssigneeId() {
        return AssigneeId;
    }

    public void setAssigneeId(String assigneeId) {
        AssigneeId = assigneeId;
    }

    public Object getGroupTaskId() {
        return GroupTaskId;
    }

    public void setGroupTaskId(Object groupTaskId) {
        GroupTaskId = groupTaskId;
    }

    public Boolean isGroup() {
        return IsGroup;
    }

    public void setIsGroup(Boolean isGroup) {
        IsGroup = isGroup;
    }

    public Integer getTypeId() {
        return TypeId;
    }

    public void setTypeId(Integer typeId) {
        TypeId = typeId;
    }

    public Integer getImplementationTypeId() {
        return ImplementationTypeId;
    }

    public void setImplementationTypeId(Integer implementationTypeId) {
        ImplementationTypeId = implementationTypeId;
    }

    public String getModelCodeId() {
        return ModelCodeId;
    }

    public void setModelCodeId(String modelCodeId) {
        ModelCodeId = modelCodeId;
    }

    public Object getAssigneeUnitId() {
        return AssigneeUnitId;
    }

    public void setAssigneeUnitId(Object assigneeUnitId) {
        AssigneeUnitId = assigneeUnitId;
    }

    public String getSurveyPointId() {
        return SurveyPointId;
    }

    public void setSurveyPointId(String surveyPointId) {
        SurveyPointId = surveyPointId;
    }

    public Integer getVehiclePosition() {
        return VehiclePosition;
    }

    public void setVehiclePosition(Integer vehiclePosition) {
        VehiclePosition = vehiclePosition;
    }

    public Integer getGasAmount() {
        return GasAmount;
    }

    public void setGasAmount(Integer gasAmount) {
        GasAmount = gasAmount;
    }

    public Object getIsInTraffic() {
        return IsInTraffic;
    }

    public void setIsInTraffic(Object isInTraffic) {
        IsInTraffic = isInTraffic;
    }

    public Integer getSpeedometer() {
        return Speedometer;
    }

    public void setSpeedometer(Integer speedometer) {
        Speedometer = speedometer;
    }

    public Object getVehiclePositionDescription() {
        return VehiclePositionDescription;
    }

    public void setVehiclePositionDescription(Object vehiclePositionDescription) {
        VehiclePositionDescription = vehiclePositionDescription;
    }

    public String getSignUserId() {
        return SignUserId;
    }

    public void setSignUserId(String signUserId) {
        SignUserId = signUserId;
    }

    public Object getStateNumber() {
        return StateNumber;
    }

    public void setStateNumber(Object stateNumber) {
        StateNumber = stateNumber;
    }

    public String getCarrierId() {
        return CarrierId;
    }

    public void setCarrierId(String carrierId) {
        CarrierId = carrierId;
    }

    public String getShippingAgentId() {
        return ShippingAgentId;
    }

    public void setShippingAgentId(String shippingAgentId) {
        ShippingAgentId = shippingAgentId;
    }

    public String getFirstDriverId() {
        return FirstDriverId;
    }

    public void setFirstDriverId(String firstDriverId) {
        FirstDriverId = firstDriverId;
    }

    public Object getSecondDriverId() {
        return SecondDriverId;
    }

    public void setSecondDriverId(Object secondDriverId) {
        SecondDriverId = secondDriverId;
    }

    public Integer getTransportationTypeId() {
        return TransportationTypeId;
    }

    public void setTransportationTypeId(Integer transportationTypeId) {
        TransportationTypeId = transportationTypeId;
    }

    public Integer getTransportId() {
        return TransportId;
    }

    public void setTransportId(Integer transportId) {
        TransportId = transportId;
    }

    public String getFromUnitId() {
        return FromUnitId;
    }

    public void setFromUnitId(String fromUnitId) {
        FromUnitId = fromUnitId;
    }

    public String getToUnitId() {
        return ToUnitId;
    }

    public void setToUnitId(String toUnitId) {
        ToUnitId = toUnitId;
    }

    public Object getFromUserId() {
        return FromUserId;
    }

    public void setFromUserId(Object fromUserId) {
        FromUserId = fromUserId;
    }

    public Object getToUserId() {
        return ToUserId;
    }

    public void setToUserId(Object toUserId) {
        ToUserId = toUserId;
    }

    public Object getSignUnitId() {
        return SignUnitId;
    }

    public void setSignUnitId(Object signUnitId) {
        SignUnitId = signUnitId;
    }
}
