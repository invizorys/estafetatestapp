package com.invizorys.estafetatestapp.api;

import com.invizorys.estafetatestapp.entity.SurveyTask;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Paryshkura Roman on 21.05.2015.
 */
public interface EstafetaApi {

    String BASE_URL = "http://rem.estafeta.org:9081/amt";

    @GET("/api/mobilesurveytasks/gettabletsurveytasks")
    void getTableSurveyTasks(@Query("userId") String userId,
                             @Query("effectivePlannedStartDate") String effectivePlannedStartDate,
                             @Query("expirationPlannedStartDate") String expirationPlannedStartDate,
                             @Query("startRowVersion") int startRowVersion, @Query("stateId") int stateId,
                             @Query("count") int count, Callback<List<SurveyTask>> surveyTaskCallback);
}
